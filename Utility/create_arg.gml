/// @function create_arg(x, y, object, layer,...args)
/// @param x
/// @param y
/// @param layer
/// @param object
/// @param ...any_arguments

var n = argument_count - 4;
//arg = undefined;
arg_count = n;
for (var i = 0; i < n; i++) {arg[i] = argument[4 + i];}
var r = instance_create_layer(argument[0], argument[1], argument[2], argument[3]);
for (var i = 0; i < n; i++) {arg[i] = 0;}
arg_count = undefined;
return r;

