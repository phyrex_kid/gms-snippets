///round_n(float, round_to)
///@param float
///@param round_to
//округляет до ближайшего второго аргумента

return round(argument[0] / argument[1]) * argument[1];