///ds_list_string(ds_list,breakets)
/// @param ds_list
// @param []		do we need breakets [ a, b, c ]

// writes ds_list as human readable string

var _list=argument[0];
var _breakets=argument[1];
var _str = "";
/*
if _breakets _str+="[";
for (var _l = 0; _l < list_sz(_list); ++_l) {
	//if ds_exists(_list[| _l], ds_type_list) _str += ds_list_string(_list[|_l],_breakets);
	_str += string(_list[|_l]);
	_str += ",- "
}
if _breakets _str+="]";

return _str;

*/

var show_map = ds_map_create();
ds_map_add_list(show_map,"m",_list);
_str=json_encode(show_map);
//ds_map_clear(show_map);
return _str;