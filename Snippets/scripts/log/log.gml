/// @function log(txt1, ...args)
/// @param maker_1
/// @param ...maker_n

var r = " - "+string(object_name)+" LOG: "+string(argument[0]);

for (var i = 1; i < argument_count; i++) {
    r+=", " + string(argument[i]);
}

show_debug_message(r);



