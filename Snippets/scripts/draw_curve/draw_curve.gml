///draw_curve(x1,y1,x2,y2,direction,detail,angle)
//
//  Draws a curve between two points with a given starting angle.
//
//      x1,y1       position of start of curve, real
//      x2,y2       position of end of curve, real
//      direction   start angle of the curve, real
//      detail      number of segments in the curve, real
//      angle       angle of the curve, real
//      width       width of the curve, real
//
/// GmLscripts.com/license

    var x1, y1, x2, y2, start_angle, detail, dist, dist_ang, inc, draw_x, draw_y, angle, ang_x, ang_y, width;
    x1 = argument[0];
    y1 = argument[1];
    x2 = argument[2];
    y2 = argument[3];
    start_angle = argument[4];
    detail = argument[5];
	angle = argument[6];
	width = argument[7];
	
	ang_x = lengthdir_x(width,angle+90);
	ang_y = lengthdir_y(width,angle+90);

    dist = point_distance(x1,y1,x2,y2);
    dist_ang = angle_difference(point_direction(x1,y1,x2,y2),start_angle);
    inc = (1/detail);

    draw_primitive_begin(/*pr_linestrip*/pr_trianglestrip);
    for (i=0; i<1+inc; i+=inc) {
        draw_x = x1 + (lengthdir_x(i * dist, i * dist_ang + start_angle));
        draw_y = y1 + (lengthdir_y(i * dist, i * dist_ang + start_angle));
        draw_vertex(draw_x,draw_y);
		draw_vertex(draw_x+ang_x,draw_y+ang_y);
    }
	
	/*for (i=1+inc; i>0; i-=inc) {
        draw_x = x1 + (lengthdir_x(i * dist, i * dist_ang + start_angle));
        draw_y = y1 + (lengthdir_y(i * dist, i * dist_ang + start_angle));
        draw_vertex(draw_x,draw_y-10);
    }*/
	
	//draw_vertex(draw_x,draw_y-10);
    draw_primitive_end();
    return 0;
