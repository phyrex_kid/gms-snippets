/// move_sin(distance, time, time_passed, and_back)
/// @param distance
/// @param time
/// @param time_passed
/// @param and_back

var ml=1;
if argument[3]=1 ml=-2;
var d = (pi*ml)/argument[1]*(argument[1]-argument[2]);
return sin(d)*(argument[0]/(argument[1]*2/pi));

//сколько времени всего
//сколько прошло
//на сколько надо продвинуться
//возвращает сколько прошел в этот степ


// move_sin(from, to, duration, offset)
// @param from
// @param to
// @param duration
// @param offset

// Returns a value that will wave back and forth between [from-to] over [duration] seconds
// Examples
//      image_angle = move_sin(-45,45,1,0)  -> rock back and forth 90 degrees in a second
//      x = move_sin(-10,10,0.25,0)         -> move left and right quickly

// Or here is a fun one! make an object be all squishy!! ^u^
//      image_xscale = move_sin(0.5, 2.0, 1.0, 0.0)
//      image_yscale = move_sin(2.0, 0.5, 1.0, 0.0)

//a4 = (argument1 - argument0) * 0.5; 
//return argument0 + a4 + sin((((current_time * 0.001) + argument2 * argument3) / argument2) * (pi*2)) * a4;



