/// draw_set(hor, ver, color, font)
/// @arg hor[1-2-3]
/// @arg ver[1-2-3]
/// @arg color
/// @arg font


draw_set_halign(argument[0]-1);
draw_set_valign(argument[1]-1);
draw_set_color(argument[2]);
draw_set_font(argument[3]);


