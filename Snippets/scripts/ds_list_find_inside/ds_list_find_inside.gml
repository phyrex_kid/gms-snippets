///ds_list_find_inside(main_list,...values)
/// @param main_list
/// @param ...values

// find a value in the list inside of the list inside of the list .... inside of the list.
// list_val(list_val(list_val(argument[0],argument[1]),argument[2]),argument[3])


var curr_list=argument[0];

for (var _level = 0; _level < argument_count-1; ++_level) {
	curr_list = list_val(curr_list,argument[_level+1]);
}

return curr_list;

