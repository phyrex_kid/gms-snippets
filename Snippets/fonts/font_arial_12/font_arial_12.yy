{
    "id": "cb9f6ebb-6361-4211-bafa-a8ad20f9b9ca",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_arial_12",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3579489a-bedc-4dda-81cb-746b45761878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 249,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d8967af0-cb9c-438a-8f00-867581a63912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 229,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8960c971-7d9f-431a-a02a-7183904a5334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 2,
                "shift": 6,
                "w": 6,
                "x": 169,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c94457e6-7076-498f-9df0-14fdd1a62a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 122,
                "y": 22
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8c549859-0550-489a-b8fd-a93c04d2b446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 134,
                "y": 22
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "669e06d8-93fa-48b8-afbe-ee0a2bf4f5d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6140416b-b4ba-47ae-bbc1-29e9a09fa7d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 146,
                "y": 22
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "18a9052e-c5aa-4de8-b246-6e4156c2d8fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 2,
                "shift": 3,
                "w": 3,
                "x": 12,
                "y": 82
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "28fbf70b-a32e-4e38-9023-fce62430bcc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 6,
                "x": 153,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7b98ca43-31cc-4865-96b6-1b202e2c88af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 177,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "995ee123-844c-4672-98e1-411ee0bfdbcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 7,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c4b5a12e-ea7a-418a-bb9b-9093645dd7e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0dc4567f-22b7-4b90-950f-346bed66497a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 17,
                "y": 82
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b03ce63c-1c46-447b-a133-bb8dd5542082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 193,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e60738d3-6cc0-4274-9641-ccf15459b084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "32ebd491-4986-4773-bf82-53f9f7ac593d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 8,
                "x": 12,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2a50bf62-6a8f-437c-ac64-16611f4af973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "dc3b3619-b3ac-4249-aec0-0457a5b31133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 185,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c8629038-2887-4366-aa76-c64e33c394e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "db7ca98b-7b31-44a8-9994-99e454c995ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f10cddc3-c701-4d08-9343-9d71907ccb66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "dc277ae6-b5f5-455b-87fe-dc659258d517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c10f4100-d1ea-413f-9900-d6a6991cd3e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fc476f2f-db09-404a-b699-32899678a7a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8e764e05-5565-4838-a3f6-19c3fd7f4b30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "cf0b96ca-9ef4-4d18-905d-c87d4daaeb01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5830baa5-87d4-405b-8295-1ccf0756b0fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 215,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e0fb8157-cca8-4a47-9b7e-433eebf840a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 222,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "59fae849-f8bf-44f4-965d-19e90e554cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 244,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "aace4987-26a6-4694-bb95-5bf58673958c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fd62044d-05a4-4a2f-9e40-4a08e8dd5b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 233,
                "y": 42
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1a1141e9-f872-4ee9-814b-34c69b0f0863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 144,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b295e895-c294-4eee-ae21-aeeaf4a72fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fa0eedb5-3916-4f2d-bf35-f400eb11d36d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 11,
                "x": 109,
                "y": 22
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b776e750-835b-4f21-9347-a0b0a078fd5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 96,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7087cb93-d201-45eb-931b-6fecd144bc6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 83,
                "y": 22
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ea91429f-1c90-4ddb-badd-1bb3b9f2c110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f53345e8-6e3e-4aa9-9e7c-a543386b5b45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "615bd667-4618-474c-9ccc-5a38971091a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 70,
                "y": 22
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2e5ad487-0145-4b83-a12e-172c2a99be21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bb5a52a3-21e6-4466-9c65-5c15b440fac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c3e5d2ae-6e76-4ec2-bf5e-7fd8b7ecaf99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 236,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b19a3368-1ef1-4242-b716-0941a44e113b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 222,
                "y": 42
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3d6a87ae-70ce-420b-8162-399429d8d5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "db9c1144-56b5-44a9-b105-8b17be5febb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "90dfaae3-4d1a-4d10-8e20-c217023d3f90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2e5764be-ccca-4eb0-9375-d64fc0dc8c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fc4f7e21-23fc-4f6b-b070-7c6bb4e096ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "290e1057-7866-4808-a2f3-045e8e4c849a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1a3897f8-5ad8-4658-aeb0-3b1c428761cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8242bf7c-20ec-4809-8bdc-e80ab9096c1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e6d5857a-fb87-4db6-bea2-1c1b4e2a0ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 22
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c890b09a-3d4b-41e3-afbb-800302699c6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 10,
                "x": 218,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ac14fe34-5103-4457-a487-cf0ba04dc860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "75bd5cf6-d911-4c0b-a3b2-9fd9d05b0038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 12,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "08210941-e5c8-4fbc-a3ae-35d2a070deb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 2,
                "shift": 15,
                "w": 15,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b5f67899-f4c7-4900-8675-505e3f1d3ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 14,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "336a8895-a29f-4e2b-ad9f-959018c7a27b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 12,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c25cfab8-cb60-49f2-86c1-e82152a71656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 57,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f6036b47-00f2-4da3-a9e1-6b196c2f14bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 7,
                "x": 117,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "089b38b9-4e10-43c6-82f0-57482076d947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 243,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0283cb06-40ce-4288-acfe-8af896b3f52f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 7,
                "x": 90,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "eecaba84-7172-4dbc-a17d-e93a9444c8e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 81,
                "y": 62
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f8765d3c-be9c-4324-bb15-4afc42a26629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -2,
                "shift": 9,
                "w": 11,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6fe7ba7a-cd91-4d32-ac90-dd92dedb8bf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 2,
                "shift": 5,
                "w": 3,
                "x": 7,
                "y": 82
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0ad0c9eb-61e8-462d-b014-46a77ff707c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 42
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cc27d55b-1857-4957-bc5e-9520ab110183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "234816f1-9786-4c51-8748-c9e9df8b688b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 68,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fb0f8722-cf77-425d-8a8b-8fa2465824a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 182,
                "y": 22
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1c816fb4-d8a4-42b4-b49b-f8f0624dcd26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 230,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8cb831e6-a2d4-44fa-87b9-b7087f259726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 7,
                "x": 99,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ce3cee1a-bf00-4fa6-89b5-309aed75a9c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 206,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "dccb1f97-54f9-4ebe-bba7-ccb125e61889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bd954b53-81bc-47b7-83d6-4d0acd93af34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 208,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2b5ad8ba-221d-40de-a41b-fd88d827399f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -2,
                "shift": 4,
                "w": 7,
                "x": 126,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "abe9a31d-3c5f-43ff-a2b9-00ad17e81050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6723936b-cd0a-4da5-8c26-4c3d7d812120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 201,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fc3ce921-420f-4df9-ac29-bc74b0dee0ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "680b0e1d-7df5-49d6-9692-3d724738b0cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "cd17571d-873f-493d-aeb8-c83b9ea1a294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 241,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4a7e1829-e96e-4ca5-be3d-5a8e347787df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 194,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ed7a1e83-bbb4-4f6a-b95c-a05a04279d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "56f76b16-9fbd-48bd-a097-6289b8eff5b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 7,
                "x": 135,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e03df06c-9514-4521-9be6-e0f972f290bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "33a715f4-989a-4088-afea-e23d9d9fc83d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 6,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0ada7990-8a00-41d5-8406-ab7c9d43cba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ef0d6502-5b90-459f-9018-c7a213c0bc07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 52,
                "y": 62
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bccc3b3f-a391-4115-8cea-11a8f8f82b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 30,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fa347cc0-59e6-4523-8270-15d331bfbec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 158,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5e47d47a-730d-4e95-9e27-77aca09006d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9507f56a-b4e9-4031-b0ba-04447a125082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 90,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "427583e6-5d29-4e7e-8ccb-aba57b26ee3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 8,
                "x": 22,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d4db0f97-aae5-4d3b-a7e0-6bafd1a7a34c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 22,
                "y": 82
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1099e456-ea91-40ce-871d-fc7944fdbf49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": -2,
                "shift": 5,
                "w": 7,
                "x": 108,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8265aaf7-1f5d-4532-87f7-14111ef4bcce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 42
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": true,
    "kerningPairs": [
        {
            "id": "6eaa1943-1e46-4fd1-b489-b30c559b9fda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "0aeef24f-d69d-4827-a3ee-15d7b37cb284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "99400b0c-e999-4127-acc3-6986671018b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "5386da1a-a28e-4fb0-a9f1-2b6da95c7fc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "c20fd5d1-3c35-4a16-a0fc-aecf610bf58c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "d6936715-21d4-46cb-9a8e-e3400fb93528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "e3509c7c-3d7f-4bfe-ae81-23bbd41722a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "2dc5296a-9a3e-4e0c-8bd6-393f62ce7801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "14b664ca-44c1-4edf-b500-8c7e02d4f49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "155397e0-666d-488b-95e8-ef2bcf8a4845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "7e0d6d78-6c14-4537-9701-9ebe4fd2f4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "eb7393b6-7fd4-4f54-a6c3-400bada7c1ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "763ba523-ffac-460b-84e4-7e94a0cca24d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "c64f4293-192e-446a-80d9-b2b94ab42d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "33675659-5462-41ef-9659-ee73dd416a56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "a943c11f-0c23-49ec-a324-4b011db674d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "5660c72d-6a0f-422b-ba53-42fd965ddb0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "feaf2abd-7735-4009-bee6-6ddf6f0a512f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "006dbe3f-1c9b-4c92-a35f-721a5af088e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "f254d3ee-ae49-4f33-8cbc-1118b493dd94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "28be8176-dedd-43e2-b556-de54d45f811c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "e48b2528-de32-495b-ab27-7560b017cce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "ad814d6a-d01f-4595-924e-ac3bf153a28d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "04b90657-b887-4a19-8459-0d2ea5c6d781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "4935d95a-9d20-447e-a566-e94e0e4c4131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "0f30fa39-0541-4f02-919a-c9a681f37c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "016b96d2-4c56-408a-93e5-9de3ac5450b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "6e723a93-1f97-410d-8240-0300564bdfbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "67b8a9ed-1c0f-491b-9f70-aaba493715b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "4599a84e-23c3-4dd9-96d3-4342a23d2065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "b467a0fe-1fa8-49d1-801a-811ede7aca65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "674dec2b-2edf-4d51-9f11-f796da935dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "35f0dbb4-0ad6-44cd-b32c-281d9f4fc29f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "7889adc7-1c6e-4479-bc20-12e107d5364e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "dc200328-e971-495a-85ad-c358890f0162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "4aa3d3b2-5284-44b4-8bb6-11ef8a6e4558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "58f25666-3498-467a-8856-55ca862cb38a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "fb38ac60-a8ab-4c66-ba99-c2911581887f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "54339365-4b98-42a2-92d8-af2a3f3c09ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "9dfdbf96-f52f-4402-9746-de4800bba7f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "15976a03-eae5-424c-ac06-499580a663ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "5bd1f376-96a8-4c97-9c6f-73234b795065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "701d752e-be51-4360-9c29-df789fcf9ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "3c65a216-ed2b-4f5e-af46-94af6937c692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "de26118c-1269-4e91-99bd-b2f36c910401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "4a93ffbf-f591-4bfe-bc50-77cfaff3f436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "c4ecc12c-9207-4ca1-a858-fcca3676ebe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "8d9022b4-7dfd-4bee-9c5a-481c9af7d347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "53816314-512b-4455-97f7-92ea0068a708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "0677ff31-7aa4-470e-ab7f-a3cba7febc7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "053268e5-f357-420e-9ca1-21ecbae390fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}