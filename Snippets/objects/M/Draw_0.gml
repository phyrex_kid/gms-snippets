///  @description sample text 

//draw_set(2,2,c_red,font0);
//draw_text(cam[X]+50, cam[Y]+10, cam[X] );

if fade_mode=fade.in {
	fade_alpha+=1/fade_time;
	draw_set_alpha(fade_alpha);
	draw_set_color(fade_color);
	draw_rectangle(cam[X],cam[Y],cam[X]+cam[W],cam[Y]+cam[H],0);
	draw_set_alpha(1);
	if fade_alpha>=1 {
		fade_alpha=1;
		room_goto(next_room);	
	}
} else if fade_mode=fade.out {
	fade_alpha-=1/fade_time;
	draw_set_alpha(fade_alpha);
	draw_set_color(fade_color);
	draw_rectangle(cam[X],cam[Y],cam[X]+cam[W],cam[Y]+cam[H],0);
	draw_set_alpha(1);
	if fade_alpha<=0 {
		fade_alpha=0;
		fade_mode=fade.no;
	}
}

if shade_mode=shade.in {
	shade_alpha+=shade_alpha_dest/shade_time;
	draw_set_alpha(shade_alpha);
	draw_set_color(shade_color);
	draw_rectangle(cam[X],cam[Y],cam[X]+cam[W],cam[Y]+cam[H],0);
	draw_set_alpha(1);
	if shade_alpha>=shade_alpha_dest {
		shade_alpha=shade_alpha_dest;
		shade_mode=shade.yes;	
	}
} else if shade_mode=shade.yes {
	draw_set_alpha(shade_alpha);
	draw_set_color(shade_color);
	draw_rectangle(cam[X],cam[Y],cam[X]+cam[W],cam[Y]+cam[H],0);
	draw_set_alpha(1);
} else if shade_mode=shade.out {
	shade_alpha-=shade_alpha_dest/shade_time;
	draw_set_alpha(shade_alpha);
	draw_set_color(shade_color);
	draw_rectangle(cam[X],cam[Y],cam[X]+cam[W],cam[Y]+cam[H],0);
	draw_set_alpha(1);
	if shade_alpha<=0 {
		shade_alpha=0;
		shade_mode=shade.no;
	}
}


