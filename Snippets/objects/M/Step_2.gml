///  @description Camera movement

#region CAMERA MOVING

if cam_moving {
	cam[X]+=move_sin(cam_dest[X], cam_time, cam_time_pass, 0);
	cam[Y]+=move_sin(cam_dest[Y], cam_time, cam_time_pass, 0);
	cam_time_pass++;
	if cam_time_pass>=cam_time {
		blocked = 0;
		cam_moving = 0;
	}
}

#endregion

#region ZOOM

if !blocked  {
	if mouse_wheel_down() {
		cam[Y]+=50;
	} else if mouse_wheel_up() {
		cam[Y]-=50;
	}
}

#endregion
 
camera_set_view_size(view_camera[0],cam[W],cam[H]);
camera_set_view_pos(view_camera[0],cam[X],cam[Y]);