///  @description Globals

#region MAIN

debug=false;					// DEBUG MODE   ON/OFF

blocked = 0;					// game is freezed
sound=true;						// sound is on/off
check[1]=0;

show_debug_overlay(debug);		// show dubug info
//window_set_fullscreen(!debug);	// go fullscreen

//instance_create_layer(x,y,layer,obj_gmlive);

#endregion

#region MACROS

global.g_ct_argument[9]=0;
global.g_ct_count=0;

#macro arg global.g_ct_argument
#macro arg_count global.g_ct_count
#macro object_name object_get_name(object_index) 
#macro mx mouse_x
#macro my mouse_y
#macro X 0
#macro Y 1
#macro W 2
#macro H 3

#macro list_val ds_list_find_value
#macro list_ind ds_list_find_index
#macro list_add ds_list_add
#macro list_sz  ds_list_size
#macro list_vals ds_list_find_inside

#macro sec room_speed // one second

#endregion

#region CAMERA
camera = camera_create();
cam[Y] = 0;
cam[W] = room_width;
cam[H] = cam[W]/room_width*room_height;
rnd=1;
cam_move_x = 0;
cam_move_y = 0;
cam_moving = 0;
cam_time  = 0;
cam_time_pass = 0;
cam_speed  = 50;
cam_dest[Y] = 0;
#endregion

#region COLORS & FONTS
color_dkgray  = make_color_rgb(36,36,45);
color_gray    = make_color_rgb(102,102,102);
color_white   = make_color_rgb(239,239,239);
color_purple  = make_color_rgb(61,6,104);
color_dkblue  = make_color_rgb(3,66,107);
color_button1 = make_color_rgb(0,83,83);
color_button2 = make_color_rgb(0,123,123);
color_button3 = make_color_rgb(76,105,255);
color_active[0] = c_white;
color_active[1] = c_yellow;
inactive_color = c_gray;
color_target_random =		c_yellow;
color_target_selective =	c_blue;
color_legal_target =		c_lime;
color_blocker =				c_red;
color_blue =				c_blue;
color_damage_to_dead =		c_ltgray;

#endregion

#region FADING

enum fade {
	out,
	in,
	no,
}
fade_mode=fade.no;
fade_color = c_black;
fade_time = sec*0.35;
fade_alpha = 0;
next_room = room_menu;

enum shade {
	out,
	in,
	no,
	yes
}
shade_mode		= shade.no;
shade_color		= c_black;
shade_time		= sec*0.35;
shade_alpha		= 0;
shade_alpha_dest= 0.7;

#endregion
	
#region TEXT
txt_coin[0] = "Heads! You goes first.";
#endregion








