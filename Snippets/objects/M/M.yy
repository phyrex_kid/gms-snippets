{
    "id": "b16f2d2e-cf10-4883-a76a-14af62106c8f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "M",
    "eventList": [
        {
            "id": "7e87c47e-8441-48c7-86b2-338800be2af0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b16f2d2e-cf10-4883-a76a-14af62106c8f"
        },
        {
            "id": "e4506a0e-6c18-4c96-a26d-a77f88f0ee2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b16f2d2e-cf10-4883-a76a-14af62106c8f"
        },
        {
            "id": "774c6174-c620-41e4-b03e-8f77615408db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b16f2d2e-cf10-4883-a76a-14af62106c8f"
        },
        {
            "id": "9725e588-d112-4416-b218-0063cb367631",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "b16f2d2e-cf10-4883-a76a-14af62106c8f"
        },
        {
            "id": "a52c85d5-3f89-40eb-a0cc-de65a20a58b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "b16f2d2e-cf10-4883-a76a-14af62106c8f"
        },
        {
            "id": "a38c809f-354a-4b97-9fbb-72000686a76e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 106,
            "eventtype": 9,
            "m_owner": "b16f2d2e-cf10-4883-a76a-14af62106c8f"
        },
        {
            "id": "c0a54cec-d39f-4e75-9df9-55feb95e6a9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "b16f2d2e-cf10-4883-a76a-14af62106c8f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}