draw_self();				//sprite 

if show_desc {				//description if any
	draw_set(desc_hallign,desc_vallign,desc_color,desc_font);
	draw_text(x-x_off+desc_offset_x,y-y_off+desc_offset_y,description);
}

draw_set(line_w,line_h,color,font);	// line 
draw_text(x,y,full_line);  


if extended { //dropdown list
    
    if mouse_check_button_pressed(mb_left) and (mx>x-sprite_width/2 and my>y+drop_height/2 and mx<x+sprite_width/2 and my<y+drop_height/2+drop_height*list_sz(droplist)) {
        //check if we scroll a list
        if list_sz(droplist)>list_lenght {last_y=my; press_y=my;}
    } else if mouse_check_button(mb_left) and (mx>x-sprite_width/2 and my>y+drop_height/2 and mx<x+sprite_width/2 and my<y+drop_height/2+drop_height*list_sz(droplist)) {
        //elements of list being selected
        choosed=((my-(y+drop_height/2)) div drop_height) + begin_list;
        if slider_height>0 {//scroll the list
			if abs(last_y-my)>drop_height {
				begin_list+=sign(last_y-my);
				last_y=my;
				scr_droplist_scroll();
			}
		}
    } else if mouse_check_button_released(mb_left) and (mx>x-sprite_width/2 and my>y+drop_height/2 and mx<x+sprite_width/2 and my<y+drop_height/2+drop_height*list_sz(droplist)) {
        //we have choose an elements from the list; assign it and close dropdown
        if list_sz(droplist)>list_lenght and abs(my-press_y)>drop_height/2 {
        
        } else {
            scr_droplist_choose();
            scr_droplist_close();
        }
    } else if (mouse_check_button_pressed(mb_left) /*or mouse_check_button_released(mb_left)*/) and (mx<x-sprite_width/2 or my<y-drop_height/2 or mx>x+sprite_width/2 or my>y+drop_height/2+drop_height*list_sz(droplist)) {
        //click outside of the list
        scr_droplist_choose();
        scr_droplist_close();
    }
    
    for(var i=0; i<min(list_sz(droplist),list_lenght); i++) {
        //backgrounds
        if choosed!=i+begin_list draw_set_color(M.color_button3);
        else draw_set_color(c_orange);
        draw_rectangle(x-x_off/*sprite_width/2*/,y-drop_height/2+drop_height*(i+1),x-x_off+sprite_width/*+sprite_width/2*/,y+drop_height/2+drop_height*(i+1),0);
        draw_set_color(c_white);
        draw_text(x,y+drop_height*(i+1),list_val(droplist,i+begin_list));
    }
    
    //mouse wheel scroll
    if mouse_wheel_down() {
        if slider_height>0 {
            begin_list+=1;
            scr_droplist_scroll();
        }
        choosed=clamp(choosed+1,0,list_sz(droplist)-1);
    } else if mouse_wheel_up() {
        if slider_height>0 {
            begin_list-=1;
            scr_droplist_scroll();
        }
        choosed=clamp(choosed-1,0,list_sz(droplist)-1);
    }
    
    //draw slider
    if slider_height>0 {
        draw_set_color(c_silver);
        draw_rectangle(x+sprite_width/2+1,y+drop_height/2,x+sprite_width/2+slider_width,y+drop_height/2+drop_height*list_lenght,0);
        draw_set_color(M.color_dkgray);
        draw_rectangle(x+sprite_width/2+1,y+drop_height/2+slider_pos,x+sprite_width/2+slider_width,y+drop_height/2+slider_height+slider_pos,0);
    }

}

