{
    "id": "45e0e8c3-9a6a-442c-8fe1-981078d89b84",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Droplist",
    "eventList": [
        {
            "id": "c64f9a12-d62b-4b16-9e40-0091c65e059f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45e0e8c3-9a6a-442c-8fe1-981078d89b84"
        },
        {
            "id": "9044f9fd-b60f-47ba-a084-c6102f89ac8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "45e0e8c3-9a6a-442c-8fe1-981078d89b84"
        },
        {
            "id": "ff78812f-bc6e-4846-9349-eb8f00b7ccea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "45e0e8c3-9a6a-442c-8fe1-981078d89b84"
        },
        {
            "id": "789f4edd-d38e-4bb6-a024-dc781255a0bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "45e0e8c3-9a6a-442c-8fe1-981078d89b84"
        },
        {
            "id": "0192e0e7-ab71-4af8-be17-9b6bf03eca37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "45e0e8c3-9a6a-442c-8fe1-981078d89b84"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7f07e6f1-e315-4394-89d5-a8cc1e5696dc",
    "visible": true
}