/// @description dropdown stats

#region SWITCH INDEX

switch index {

case 0: 
sprite=noone;
font=font_arial_12;
line="Sample Text";
color=c_white;
custom_size=1;
width=100;	
height=sprite_height; 
show_desc = 1;
description = "Sample Text:";
break;

}

#endregion


//recreate droplist?
scr_droplist_create();

sprite_index=sprite;
image_index=image;
image_speed=0;
visible=1;

if start {//change only first time
	
	//description on the left
	if sub_index[1]=1 { 
		desc_offset_x	= -sprite_width;
		desc_offset_y	= sprite_height/2;
		desc_color		= c_white;
		desc_font		= font0;
		desc_hallign	= 1;
		desc_vallign	= 2;
	}
	
	//x and y offset
	x_off = sprite_get_xoffset(sprite_index);
	y_off = sprite_get_yoffset(sprite_index);
	if custom_size {
		image_xscale=1/sprite_width*width;
		image_yscale=1/sprite_height*height;
		x_off*=image_xscale;
		y_off*=image_yscale;
	}
	drop_height=sprite_height-7;
}

if inactive {
    image_blend=c_ltgray;
    image_alpha=0.5;
	color=c_gray;
} else {
    image_blend=c_white;
    image_alpha=1;
	color=c_white;
}
start=false;

full_line=line;
