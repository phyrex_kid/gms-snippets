index=arg[0];
sub_index[0]=arg[1];
sub_index[1]=arg[2];
sub_index[2]=arg[3];

focused = 0;
inactive = false;
start = true;

line="Txt";
last_line="";
description="dscription";
font=font0;
color=c_white;
normal_color=c_black;
drow_color=make_color_rgb(199,7,51);
text=0;
placeholder ="placeholder";
length = 25;
x_off = 0;
y_off = 0;
show_desc		= true;
desc_offset_x	= 0;
desc_offset_y	= -2;
desc_color		= c_black;
desc_font		= font_arial_12;
desc_hallign	= 1;
desc_vallign	= 3;

image_speed=0;
image_index=0;

text=create_arg(0,0,M.dpt[0],TextInput,id);

alarm[0]=1;
visible=0;