///  @description switch index


#region SWITCH INDEX
//line='';
//description='';
//font='';
//color='';
//normal_color='';
//drow_color='';
//text=0;
//placeholder ='';


switch index {

case control_text.maker_id: //card maker - new card id
placeholder="ID";
description="New card ID";
font=font0;
normal_color=c_white;
drow_color=make_color_rgb(199,7,51);
length = 3;
text.under[1] = "_";
sprite_index = noone;
break;

}


//x and y offset
x_off = sprite_get_xoffset(sprite_index);
y_off = sprite_get_yoffset(sprite_index);

//description on the left
if sub_index[1]=1 { 
	desc_offset_x	= -Droplist.sprite_width;
	desc_offset_y	= Droplist.sprite_height/2;
	desc_color		= c_white;
	desc_font		= font0;
	desc_hallign	= 1;
	desc_vallign	= 2;
}

if start {
	color=normal_color;
	line=placeholder;
	start = false;
}

if inactive {
    image_blend=c_ltgray;
    image_alpha=0.5;
	color=c_gray;
} else {
    image_blend=c_white;
    image_alpha=1;
	color=c_white;
}

visible=1;
