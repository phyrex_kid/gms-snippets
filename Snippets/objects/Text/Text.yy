{
    "id": "1b11c806-2522-4354-8647-60271e345b7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Text",
    "eventList": [
        {
            "id": "18e3911a-fa30-4c00-b239-6ee9747154f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1b11c806-2522-4354-8647-60271e345b7c"
        },
        {
            "id": "93a05d3c-c29f-420f-9467-68687bad7755",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "1b11c806-2522-4354-8647-60271e345b7c"
        },
        {
            "id": "f4c5a98b-fac7-4deb-b2c7-0ab84d45a481",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "1b11c806-2522-4354-8647-60271e345b7c"
        },
        {
            "id": "9d278488-c24c-4b5a-8ba1-e494bf10aa6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1b11c806-2522-4354-8647-60271e345b7c"
        },
        {
            "id": "97a9dd70-c0b1-4b94-9748-181b9b346be0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "1b11c806-2522-4354-8647-60271e345b7c"
        },
        {
            "id": "26b03e34-e259-41f5-a62b-b6618b9544b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1b11c806-2522-4354-8647-60271e345b7c"
        },
        {
            "id": "ee11294f-d0e2-4108-86e4-089485f14d14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1b11c806-2522-4354-8647-60271e345b7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d8fddc80-8f9e-4126-ab8e-0c6b7edbfc25",
    "visible": true
}