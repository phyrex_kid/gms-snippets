if show_desc {
	draw_set(desc_hallign,desc_vallign,desc_color,desc_font);
	//draw_set_alpha(0.6);
	draw_text(x-x_off+desc_offset_x,y-y_off+desc_offset_y,description);
	//draw_set_alpha(1);
}

draw_self();

draw_set(1,1,color,font);
draw_text_ext(x-x_off+6,y-y_off+4,line,26,sprite_width-12);

