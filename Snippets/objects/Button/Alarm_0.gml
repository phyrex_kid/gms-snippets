///  @description switch index


#region SWITCH INDEX

//width=100;
//height=40;
//custom_size=0;
//blend=c_white;

switch index {
case 1: //card maker - create new
	sprite=noone;
	font=font0;
	line="NEW";
	color=c_white;
break;

case 7: //card maker - switch to base
	sprite=noone;
	font=font0;
	line="CARD BASE";
	color=c_white;
	width=140;
	height=sprite_height;
	custom_size=1;
break;

}

#endregion


sprite_index=sprite;
image_index=0;
image_speed=0;
visible=1;

if start {//change only first time
	//x and y offset
	x_off = sprite_get_xoffset(sprite_index);
	y_off = sprite_get_yoffset(sprite_index);
	
	if custom_size {
		image_xscale=1/sprite_width*width;
		image_yscale=1/sprite_height*height;
	}
}

if inactive {
    image_blend=c_ltgray;
    image_alpha=0.5;
	color=c_gray;
} else {
    image_blend=c_white;
    image_alpha=1;
	color=c_white;
}

start=0;