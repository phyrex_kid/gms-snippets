{
    "id": "391a5d1a-da31-47d0-b13b-b3ac600746fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Button",
    "eventList": [
        {
            "id": "e8f045d4-e18a-4bbc-8e1b-afb8dffa47e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "391a5d1a-da31-47d0-b13b-b3ac600746fd"
        },
        {
            "id": "eb374ce6-10d8-4085-b0b6-7b4015cb1832",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "391a5d1a-da31-47d0-b13b-b3ac600746fd"
        },
        {
            "id": "2c002ec2-ff03-4915-8ac6-f5f8e3645416",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "391a5d1a-da31-47d0-b13b-b3ac600746fd"
        },
        {
            "id": "73af69f8-462a-4cb2-84cd-5654658ae4b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "391a5d1a-da31-47d0-b13b-b3ac600746fd"
        },
        {
            "id": "dd05efdd-f676-4b9a-bcb1-f8b09c678d1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "391a5d1a-da31-47d0-b13b-b3ac600746fd"
        },
        {
            "id": "55cc2565-2d04-4351-b6f0-ff1aca0250b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "391a5d1a-da31-47d0-b13b-b3ac600746fd"
        },
        {
            "id": "7a955962-feec-4c32-b224-e5c11e12d516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "391a5d1a-da31-47d0-b13b-b3ac600746fd"
        },
        {
            "id": "ed94e727-3de7-4801-943c-39fdf303f1b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "391a5d1a-da31-47d0-b13b-b3ac600746fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "75e3470a-6188-478c-902b-77cbcccdb43e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "index",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "189254c8-3d41-43f7-84da-596ad4a310fd",
    "visible": true
}