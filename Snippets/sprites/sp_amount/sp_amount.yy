{
    "id": "7f07e6f1-e315-4394-89d5-a8cc1e5696dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_amount",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 227,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "051db830-daff-40ac-b707-113f6e18e155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f07e6f1-e315-4394-89d5-a8cc1e5696dc",
            "compositeImage": {
                "id": "27eeb31a-6634-4eb0-9e17-5b86c8d6ee57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "051db830-daff-40ac-b707-113f6e18e155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ade783ad-e350-4135-9362-c7c1f278275e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "051db830-daff-40ac-b707-113f6e18e155",
                    "LayerId": "ce9abce0-9aca-47ac-b72c-223ae8e9b35f"
                }
            ]
        },
        {
            "id": "0a635598-a424-4b2b-a440-44d9ef6605a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f07e6f1-e315-4394-89d5-a8cc1e5696dc",
            "compositeImage": {
                "id": "2a6ba265-8062-4093-b62d-2942a14035d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a635598-a424-4b2b-a440-44d9ef6605a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bfd7d3d-f17b-4860-a440-b2a7aceb42da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a635598-a424-4b2b-a440-44d9ef6605a1",
                    "LayerId": "ce9abce0-9aca-47ac-b72c-223ae8e9b35f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "ce9abce0-9aca-47ac-b72c-223ae8e9b35f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f07e6f1-e315-4394-89d5-a8cc1e5696dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 228,
    "xorig": 114,
    "yorig": 14
}