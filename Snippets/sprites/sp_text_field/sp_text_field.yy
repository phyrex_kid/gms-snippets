{
    "id": "d8fddc80-8f9e-4126-ab8e-0c6b7edbfc25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_text_field",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 291,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "daec2b4d-9549-4bcb-b848-0b3fd45b14f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8fddc80-8f9e-4126-ab8e-0c6b7edbfc25",
            "compositeImage": {
                "id": "87212505-88d5-439b-acf0-f53a4c550e3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daec2b4d-9549-4bcb-b848-0b3fd45b14f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d81d5bf-85b3-4315-ac45-2f49c700c09e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daec2b4d-9549-4bcb-b848-0b3fd45b14f6",
                    "LayerId": "01e33895-832d-4ef5-afd3-81dc9ba38689"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "01e33895-832d-4ef5-afd3-81dc9ba38689",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8fddc80-8f9e-4126-ab8e-0c6b7edbfc25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 292,
    "xorig": 0,
    "yorig": 0
}