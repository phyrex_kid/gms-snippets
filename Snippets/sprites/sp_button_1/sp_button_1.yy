{
    "id": "189254c8-3d41-43f7-84da-596ad4a310fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_button_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a218ee9c-a4ff-4be1-bd82-fcc37e1a1b66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189254c8-3d41-43f7-84da-596ad4a310fd",
            "compositeImage": {
                "id": "78771859-59ae-4dd8-9e62-b544017db47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a218ee9c-a4ff-4be1-bd82-fcc37e1a1b66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dd6561f-ff2f-4dd0-9dfb-0a24bcfff97d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a218ee9c-a4ff-4be1-bd82-fcc37e1a1b66",
                    "LayerId": "4c2becc8-592d-4c2f-bc07-2f78999a9726"
                }
            ]
        },
        {
            "id": "190b6f22-8e9c-43b4-beb0-6fa91280a2b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189254c8-3d41-43f7-84da-596ad4a310fd",
            "compositeImage": {
                "id": "b72cb420-b35d-4e72-b4b2-b0b47dac5675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "190b6f22-8e9c-43b4-beb0-6fa91280a2b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40559826-1a77-4ee3-98c8-2e41ccc90500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "190b6f22-8e9c-43b4-beb0-6fa91280a2b4",
                    "LayerId": "4c2becc8-592d-4c2f-bc07-2f78999a9726"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "4c2becc8-592d-4c2f-bc07-2f78999a9726",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "189254c8-3d41-43f7-84da-596ad4a310fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}